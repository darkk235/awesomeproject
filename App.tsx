import axios from 'axios'
import React from 'react'
import { FlatList, Image, StatusBar, StyleSheet, Text, View, Animated } from 'react-native';

const App = () => {
  const [datas,setDatas] = React.useState([])
  const scrollY = React.useRef(new Animated.Value(0)).current

  const API_URL = 'https://jsonplaceholder.typicode.com/posts';
  const IMG_URL = 'https://th.bing.com/th/id/R.91e206fa02d81f85dd4e42ef005a88d9?rik=5MxgANLhpAlBJw&riu=http%3a%2f%2fthewowstyle.com%2fwp-content%2fuploads%2f2015%2f07%2fautunm-desktop-natural-hd-wallpapers.jpg&ehk=j7yVlSgUBySdpPIKhx0ovaCIvpI2u%2f%2bw3lfHET9HT4w%3d&risl=1&pid=ImgRaw&r=0';
  
  const SPACING = 10;
  const CARD_SIZE = SPACING * SPACING + 38

  const fecthData = async () => {
    await axios.get(API_URL)
    .then((res) => {
      const jsonData = res.data.sort((a,b) => a.id - b.id)
      setDatas(jsonData)
    })
    .catch((err) => {
      console.log(err);
    })
  }

  React.useEffect(() => {
    fecthData()
  },[])


  return (
    <View style={{flex:1}}>
      <StatusBar barStyle={'dark-content'} backgroundColor={'white'}/>
      <Image source={{uri:IMG_URL}} style={StyleSheet.absoluteFillObject} blurRadius={30}/>
      <Animated.FlatList 
        data={datas}
        keyExtractor={(item,index) => index.toString()}
        renderItem={({item,index}) => {

          const scale = scrollY.interpolate({
            inputRange:[-1,0,CARD_SIZE * index,CARD_SIZE * (index + 2)],
            outputRange:[1,1,1,0]
          })

          const opacity = scrollY.interpolate({
            inputRange:[-1,0,CARD_SIZE * index,CARD_SIZE * (index + 2)],
            outputRange:[1,1,1,0]
          })

          return (
            <Animated.View style={{backgroundColor:'white',margin:SPACING,padding:SPACING,borderRadius:10,transform:[{scale}],opacity}}>
              <Text style={{fontSize:13,color:'gray'}}>{item.userId}</Text>
              <Text style={{fontSize:13,color:'gray'}}>{item.id}</Text>
              <Text style={{fontSize:13,color:'gray'}}>{item.title}</Text>
              <Text style={{fontSize:13,color:'gray'}}>{item.body}</Text>
            </Animated.View>
          )
        }}
        onScroll={Animated.event(
          [{nativeEvent:{contentOffset:{y:scrollY}}}],
          {useNativeDriver:true}
        )}
      />
    </View>
  )
}

export default App