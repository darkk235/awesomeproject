import axios from 'axios';
import React from 'react';
import {
  FlatList,
  Text,
  View,
  Image,
  StyleSheet,
  StatusBar,
  Animated,
} from 'react-native';

const App = () => {
  const [userData, setUserData] = React.useState([]);
  const [loading, setLoading] = React.useState([]);

  const scrollY = React.useRef(new Animated.Value(0)).current;
  const SPACING = 10;
  const ITEM_SIZE = SPACING * 10 + 25;
  const IMG ='https://wallpapertag.com/wallpaper/full/2/c/d/308641-blue-sky-background-1920x1080-for-android.jpg';
  const APIDATA = 'https://jsonplaceholder.typicode.com/posts';

  const apiData = async () => {
    setLoading(true);
    await axios
      .get(APIDATA)
      .then(function (response) {
        const data = response.data;
        data.sort((a, b) => a.id - b.id);
        setUserData(data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        setLoading(!loading);
      });
  };

  React.useEffect(() => {
    apiData();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar barStyle={'dark-content'} backgroundColor={'#fff'} />
      <Image
        source={{uri: IMG}}
        style={StyleSheet.absoluteFillObject}
        blurRadius={50}
      />

      <Animated.FlatList
        data={userData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item,index}) => {
          const inputRange = [
            -1,0,
            ITEM_SIZE * index,
            ITEM_SIZE * (index + 2)
          ]
          
          const scale = scrollY.interpolate({
            inputRange,
            outputRange:[1,1,1,0]
          })

          const opacityRange = [
            -1,0,
            ITEM_SIZE * index,
            ITEM_SIZE * (index + 2)
          ]
          
          const opacity = scrollY.interpolate({
            inputRange,
            outputRange:[1,1,1,0]
          })

          return (
            <Animated.View
              style={{
                margin: SPACING,
                backgroundColor: '#fff',
                padding: SPACING,
                borderRadius: 10,
                elevation: 2,
                opacity,
                transform:[{scale}]
              }}>
              <Text style={{fontSize: 12}}>{item.userId}</Text>
              <Text style={{fontSize: 12}}>{item.id}</Text>
              <Text style={{fontSize: 12}}>{item.title}</Text>
              <Text style={{fontSize: 12}}>{item.body}</Text>
            </Animated.View>
          );
        }}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: scrollY}}}],
          {useNativeDriver: true},
        )}
      />
    </View>
  );
};

export default App;
